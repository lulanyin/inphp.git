<?php
// +----------------------------------------------------------------------
// | INPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2021 https://inphp.cc All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/MIT )
// +----------------------------------------------------------------------
// | Author: lulanyin <me@lanyin.lu>
// +----------------------------------------------------------------------
// | 
// |                          Smarty 模板引擎
// |
// +----------------------------------------------------------------------
namespace Inphp\Middlewares;

use Inphp\Config;
use Inphp\Helper\SmartyHelper;
use Inphp\Service\Context;
use Smarty;

class SmartyView
{
    public static function process(mixed $controller, string|null $method = null)
    {
        $response = Context::getResponse();
        if($response->content == null && $response->content_type == 'default')
        {
            $status = $response->status;
            $smarty_config = Config::get("private.smarty");
            if(!class_exists('Smarty'))
            {
                return;
            }
            if(!defined("SMARTY_TAGS_PARSER")){
                //添加支持自定义Smarty标签
                define('SMARTY_TAGS_PARSER', $smarty_config['dir'] ?? RUNTIME."/smarty");
            }
            $smarty = new Smarty();
            $smarty->cache_lifetime = $smarty_config['cache_lifetime'];
            $smarty->left_delimiter = $setting['left_delimiter'] ?? "{";
            $smarty->right_delimiter = $setting['right_delimiter'] ?? "}";
            $smarty->setCacheDir($smarty_config['cache_dir']);
            $smarty->setCompileDir($smarty_config['compile_dir']);
            //载入公共配置
            $configs = Config::get("public");
            $smarty->assign("configs", $configs);
            $smarty->assign("method", $response->status->method);
            $smarty->assign("domain", Config::get("domain"));
            //客户端数据
            $client = Context::getClient();
            $smarty->assign("get", $client->get);
            $smarty->assign("post", $client->post);
            $smarty->assign("cookies", $client->cookies);
            //控制器赋值的模板数据
            $dataList = getAssignData();
            $dataList = is_array($dataList) ? $dataList : [];
            foreach ($dataList as $key => $value){
                $smarty->assign($key, $value);
            }
            //控制器返回的数据
            if(!empty($response->controller_result)){
                $smarty->assign('data', $response->controller_result);
            }
            //2022.3.5 添加支持自定义function, modifier, block
            foreach (["function", "modifier", "block"] as $plugin_type) {
                if(!empty(SmartyHelper::$pliguns[$plugin_type])){
                    foreach (SmartyHelper::$pliguns[$plugin_type] as $plugin_name => $pligun){
                        $smarty->registerPlugin($plugin_type, $plugin_name, $pligun);
                    }
                }
            }
            if($status->status !== 200)
            {
                $response->withStatus($status->status);
                return;
            }
            //视图位置
            $view_dir = $status->view_dir ?? '';
            $view_dir = strrchr($view_dir, "/") == "/" ? $view_dir : "{$view_dir}/";
            //首个斜杠去掉
            $file = stripos($status->view, "/") === 0 ? substr($status->view, 1) : $status->view;
            //后缀
            $file = in_array(strrchr($file, "."), [".html", ".htm", ".tpl"]) ? $file : "{$file}.html";
            //组合路径
            $view_file = $view_dir."/".$file;
            //双斜杠转换为单斜杠
            $view_file = str_replace("//", "/", $view_file);
            if(file_exists($view_file)){
                $smarty->setTemplateDir($view_dir);
                try{
                    $response->withHTML($smarty->fetch($file));
                }catch (\Exception $exception){
                    $response->withText($exception->getMessage());
                }
            }else{
                $response->withStatus(404);
            }
        }
    }
}