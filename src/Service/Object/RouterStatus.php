<?php
// +----------------------------------------------------------------------
// | INPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2021 https://inphp.cc All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/MIT )
// +----------------------------------------------------------------------
// | Author: lulanyin <me@lanyin.lu>
// +----------------------------------------------------------------------
namespace Inphp\Service\Object;

/**
 * 路由状态
 * Class Status
 * @package Inphp\Service\Object
 */
class RouterStatus
{
    /**
     * 响应状态，目前只处理 200，404
     * @var int
     */
    public int $status = 200;

    /**
     * 消息，在发生错误的时候，可以加上
     * @var string
     */
    public string $message = "ok";

    /**
     * 自定义数据...按需处理
     * @var string
     */
    public string $state = 'controller';

    /**
     * 控制器类名
     * @var mixed
     */
    public mixed $controller = null;

    /**
     * 控制器需要执行的方法名
     * @var string
     */
    public string $method = "index";

    /**
     * 视图文件夹
     * @var mixed
     */
    public mixed $view_dir = null;

    /**
     * 视图文件名，并非全路径，相对版块路径
     * @var mixed
     */
    public mixed $view = null;

    /**
     * 进入的版块路径
     * @var string
     */
    public string $path = '';

    /**
     * 请求的 uri
     * @var string
     */
    public string $uri = '';

    /**
     * 内容响应的格式
     * @var string
     */
    public string $response_content_type = 'default';

    /**
     * Status constructor.
     * @param array $values
     */
    public function __construct(array $values)
    {
        $this->status       = $values['status'] ?? 200;
        $this->message      = $values['message'] ?? 'ok';
        $this->state        = $values['state'] ?? 'controller';
        $this->controller   = $values['controller'] ?? null;
        $this->method       = $values['method'] ?? 'index';
        $this->view_dir     = $values["view_dir"] ?? null;
        $this->view         = $values['view'] ?? null;
        $this->path         = $values['path'] ?? '';
        $this->uri          = $values['uri'] ?? '';
        $this->response_content_type = $values['response_content_type'] ?? 'default';
    }
}