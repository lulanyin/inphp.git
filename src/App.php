<?php
// +----------------------------------------------------------------------
// | INPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2021 https://inphp.cc All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/MIT )
// +----------------------------------------------------------------------
// | Author: lulanyin <me@lanyin.lu>
// +----------------------------------------------------------------------
namespace Inphp;

use Inphp\Service\Server;
use Inphp\Service\Service;

class App
{
    /**
     * @var Server|null
     */
    public static Server|null $server;

    /**
     * 服务初始化
     * @param bool $swoole
     * @param bool $ws
     * @return Service
     */
    public static function init(bool $swoole = false, bool $ws = false) : Service
    {
        //定义服务类型
        !defined("INPHP_SERVICE_PROVIDER") && define("INPHP_SERVICE_PROVIDER", $swoole ? Service::SWOOLE : Service::FPM);
        //实例化服务
        $service = new Service($swoole, $ws);
        //保存 server 对象到全局
        self::$server = $service->server;
        //
        return $service;
    }
    
    /**
     * 命令行工具
     * @return \Inphp\Service\Cmd\Server
     */
    public static function cmd(): \Inphp\Service\Cmd\Server
    {
        echo "+---------------------+\r\n";
        echo "| ♪♪♪♪♪♪ INPHP ♪♪♪♪♪♪ |\r\n";
        echo "| Think you for using |\r\n";
        echo "|    commend line     |\r\n";
        echo "|  https://inphp.cc   |\r\n";
        echo "+---------------------+\r\n";
        
        return new \Inphp\Service\Cmd\Server();
    }

    /**
     * 是否是swoole运行
     * @return bool
     */
    public static function isSwoole(): bool
    {
        return defined("INPHP_SERVICE_PROVIDER") && INPHP_SERVICE_PROVIDER == Service::SWOOLE;
    }
}