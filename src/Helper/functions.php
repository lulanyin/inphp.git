<?php
// +----------------------------------------------------------------------
// | INPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2021 https://inphp.cc All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/MIT )
// +----------------------------------------------------------------------
// | Author: lulanyin <me@lanyin.lu>
// +----------------------------------------------------------------------

/**
 * 获取 GET
 * @param string $key
 * @param mixed $default
 * @return mixed
 */
function GET(string $key, mixed $default = null) : mixed
{
    $client = \Inphp\Service\Context::getClient();
    $values = $client->get('get');
    $values = is_array($values) ? $values : [];
    return $values[$key] ?? $default;
}

/**
 * 获取 POST
 * @param string $key
 * @param mixed $default
 * @return mixed
 */
function POST(string $key, mixed $default = null) : mixed
{
    $client = \Inphp\Service\Context::getClient();
    $values = $client->get('post');
    $values = is_array($values) ? $values : [];
    return $values[$key] ?? $default;
}

/**
 * GET或POST的参数
 * @param string $key
 * @param mixed|null $default
 * @return mixed
 */
function REQUEST(string $key, mixed $default = null) : mixed
{
    $client = \Inphp\Service\Context::getClient();
    $values = $client->get('request');
    $values = is_array($values) ? $values : [];
    return $values[$key] ?? $default;
}

/**
 * 重定向
 * @param string $url
 * @param int $code
 */
function redirect(string $url, int $code = 302) : void
{
    $response = \Inphp\Service\Context::getResponse();
    $response->redirect($url, $code);
}

/**
 * 获取IP
 * @return string
 */
function getIP() : string
{
    $client = \Inphp\Service\Context::getClient();
    return $client->get('ip');
}

/**
 * 数字时间格式化为字符串时间
 * @param int $time
 * @return string
 */
function time2datetime(int $time) : string
{
    return date("Y-m-d H:i:s", $time);
}

/**
 * 字符串时间，格式化为数字时间
 * @param string $time
 * @return int
 */
function datetime2time(string $time) : int
{
    return strtotime($time);
}

/**
 * 返回一组数据，用于JSON响应
 * @param mixed $error
 * @param mixed $message
 * @param mixed $data
 * @return array
 */
function json(mixed $error = 0, mixed $message = 'success', mixed $data = null) : array
{
    $data = is_array($error) || is_object($error) ? $error : (is_array($message) || is_object($message) ? $message : $data);
    $message = is_string($error) ? $error : (is_array($message) || is_object($message) ? null : $message);
    $error = is_array($error) || is_object($error) ? 0 : (is_string($error) ? 1 : $error);
    $json = [
        "error" => $error,
        "message" => $message
    ];
    if(!is_null($data)){
        $json['data'] = $data;
    }
    return $json;
}

/**
 * HTTP Message 对象
 * @param mixed $error
 * @param mixed $message
 * @param mixed $data
 * @return \Inphp\Service\Object\Message
 */
function httpMessage(mixed $error = 0, mixed $message = 'success', mixed $data = null) : \Inphp\Service\Object\Message
{
    return new \Inphp\Service\Object\Message(json($error, $message, $data));
}

/**
 * websocket Message 对象
 * @param mixed $event
 * @param mixed $error
 * @param mixed $message
 * @param mixed $data
 * @return \Inphp\Service\Object\Message
 */
function websocketMessage(mixed $event, mixed $error = 0, mixed $message = 'success', mixed $data = null) : \Inphp\Service\Object\Message
{
    $json = json($error, $message, $data);
    $json['event'] = $event;
    return new \Inphp\Service\Object\Message($json);
}

/**
 *
 * @param string $content_type
 * @param mixed $body
 * @return \Inphp\Service\Http\Response
 */
function response(mixed $body, string $content_type = 'application/json'): \Inphp\Service\Http\Response
{
    $response = \Inphp\Service\Context::getResponse();
    return $response->withHeader("Content-Type", $content_type)->withContent($body);
}

/**
 * 保存临时模板数据
 * @param string $name
 * @param mixed $value
 */
function assign(string $name, mixed $value)
{
    $data = \Inphp\Service\Context::get('smarty_assign_data');
    $data = $data ?? [];
    $data[$name] = $value;
    \Inphp\Service\Context::set('smarty_assign_data', $data);
}

/**
 * 获取临时模板数据
 * @param string|null $name
 * @return mixed
 */
function getAssignData(string|null $name = null) : mixed
{
    $data = \Inphp\Service\Context::get('smarty_assign_data');
    $data = $data ?? [];
    if(!is_null($name))
    {
        return $data[$name] ?? null;
    }
    return $data;
}